class Vehiculo {
  bool encendido = false;

  void encender() {
    encendido = true;
  }

  void apagar() {
    encendido = false;
  }
}

class Carro extends Vehiculo{
  int kilometraje = 0;
}

void main() {
  final ford = new Carro();
  ford.encender();
}