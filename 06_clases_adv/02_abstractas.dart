abstract class Vehiculo {
  bool encendido = false;

  void encender() {
    encendido = true;
  }

  void apagar() {
    encendido = false;
  }

  bool revisarMotor();
}

class Carro extends Vehiculo{
  int kilometraje = 0;

  @override
  bool revisarMotor() {
    print('Motor ok!');
    return true;
  }
}

void main() {
  final ford = new Carro();
  ford.encender();
  ford.revisarMotor();
}