mixin Logger {
  void imprimir(String texto) {
    final hoy = DateTime.now();
    print('$hoy ::: $texto');
  }
}

mixin Logger2 {
  void imprimir2(String texto) {
    final hoy = DateTime.now();
    print('$hoy ::: $texto');
  }
}

abstract class Astro with Logger {
  String? neombre;

  Astro() {
    imprimir('---Init del Astro---');
  }

  void existo() {
    imprimir('--soy un ser celestial y existo--');
  }
}

class Asteroide extends Astro with Logger, Logger2 {
  String? nombre;
  Asteroide(this.nombre) {
    imprimir('soy $nombre');
    imprimir2('soy $nombre');
  }
}

void main() {
  final ceres = new Asteroide('Name');
}