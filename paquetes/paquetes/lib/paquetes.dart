import 'package:http/http.dart' as http;
import 'package:paquetes/clases/reqrest_respuesta.dart';

void getReqRest_service() async{
  try {
    final url = Uri.parse('https://reqres.in/api/users?page=2');
    final response = await http.get(url);
    if(response.statusCode == 200) {
      final jsonResponse = reqResRespuestaFromJson(response.body);
      print(jsonResponse.page);
      print(jsonResponse.data[0].id);
    }
  } catch (e) {
    print('Error: $e');
  }
}