import 'dart:io';
// promesas
void main() {
  print(Directory.current.path);
  // osx y linux
  //File file = new File(Directory.current.path + '/04_tipos_nocomunes/assets/personas.txt');
  // windows
  File file = new File(Directory.current.path + '\\04_tipos_nocomunes\\assets\\personas.txt');

  Future<String> f = file.readAsString();

  f.then((data) => print(data));
  f.then((print));

  print('fin del main');
}