void main() async{
  try {
    String texto = await timeout();
    print(texto);
  } catch (error) {
    print(error);
  }
}

Future<String> timeout() async {
  return Future.delayed(Duration(seconds: 3), () {
    if(1 == 1) {
      throw 'Mensaje de error';
    }

    return 'retorno del future';
  });
}