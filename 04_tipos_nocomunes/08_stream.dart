import 'dart:async';

main(){
  //final StreamController<String> streamController = StreamController();
  //final streamController = StreamController();
  // stram controleer de una suscripcion
  //final streamController = StreamController<String>();
  // stram controleer de multiple suscripciones
  final streamController = StreamController<String>.broadcast();

  streamController.stream.listen(
    (data) => print('Despegando! $data'),
    onError: (err) => print('Error! $err'),
    onDone: () => print('Mision completa!'),
    // cancelar el stream usando true
    cancelOnError: false
  );

  streamController.stream.listen(
    (data) => print('Despegando! Stream 2 $data'),
    onError: (err) => print('Error! Stream 2 $err'),
    onDone: () => print('Mision completa! Stream 2'),
    // cancelar el stream usando true
    cancelOnError: false
  );

  streamController.sink.add('Apollo 11');
  streamController.sink.add('Apollo 12');
  streamController.sink.add('Apollo 13');
  streamController.sink.addError('Hpuston, tenemos un problema');
  streamController.sink.add('Apollo 14');
  streamController.sink.add('Apollo 15');

  streamController.sink.close();

  print('fin del main');
}