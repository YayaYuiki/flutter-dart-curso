import 'dart:io';
// promesas
void main() async {
  print(Directory.current.path);

  // osx y linux
  //File file = new File(Directory.current.path + '/04_tipos_nocomunes/assets/personas.txt');
  // windows
  String path = Directory.current.path + '\\04_tipos_nocomunes\\assets\\personas.txt';
  /* leerArchivo(path).then((texto) => print(texto));
  leerArchivo(path).then(print); */
  String texto = await leerArchivo(path);
  print(texto);

  print('fin del main');
}

Future<String> leerArchivo(String path) async{
  File file = new File(path);
  return file.readAsString();
}

Future<String> funcionAsyncronaFuture(String path) async{
  return 'texto';
}