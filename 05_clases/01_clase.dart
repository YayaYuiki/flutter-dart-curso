import 'clases/persona.dart';
main(){
  final persona = new Persona(edad:33, nombre: 'Fernando');
  final persona2 = new Persona.persona30('Fernando');

  /* persona..nombre = 'Fernando'
    ..edad = 33
    ..bio = 'Cambié el valor!'; */

  print(persona);
  print(persona.bio);
  print(persona2);
}