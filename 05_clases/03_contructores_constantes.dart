class Location {
  final double? lat;
  final double? lng;

  const Location(this.lat, this.lng);
}
main(){
  final sanFrancisco1 = new Location(18.2323, 39.300);
  final sanFrancisco2 = new Location(18.2323, 39.901);
  final sanFrancisco3 = new Location(18.2323, 39.901);
  const sanFrancisco4 = const Location(18.2323, 39.300);
  const sanFrancisco5 = const Location(18.2323, 39.901);
  const sanFrancisco6 = const Location(18.2323, 39.901);

  print(sanFrancisco1 == sanFrancisco2); // false difernte memoria
  print(sanFrancisco2 == sanFrancisco3); // false difernte memoria

  print(sanFrancisco4 == sanFrancisco5); // false difernte memoria
  print(sanFrancisco5 == sanFrancisco6); // true misma memoria
}