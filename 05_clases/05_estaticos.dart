class Herramientas {
  static const List<String> listado = ['M', 'L', 'd'];
  static void imprimirListado() => listado.forEach(print);
}

void main(List<String> args) {
  Herramientas.imprimirListado();
}