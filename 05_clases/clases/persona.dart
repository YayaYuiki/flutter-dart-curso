class Persona {
  // campos o propiedades
  String? nombre;
  int? edad;
  // proiedad privada
  String _bio = 'propiedad privada';

  // get y sets
  /* String get bio {
    return _bio.toLowerCase();
  } */
  String get bio => _bio.toLowerCase();

  /* set bio(String texto) {
    _bio = texto;
  } */

  set bio(String texto) => _bio = texto;

  // constructores

  /*Persona(int edad, String nombre){
    this.edad = edad;
    this.nombre = nombre;
  } */
  //Persona(this.edad, {this.nombre = 'Maria'}); // {} -> opcional
  Persona({ this.edad = 0, required this.nombre}); // required this.value -> argumento requerido u obligatorio
  Persona.persona30(this.nombre){
    this.edad = 30;
  }

  Persona.persona40(String nombre){
    this.edad = 40;
    this.nombre = nombre;
  }

  // metodos
  @override
  String toString() => '$nombre $edad $_bio';
}