String capitalizar(String texto){
  texto = texto.toUpperCase();
  return texto;
}

Map<String, String> capitalizarMapa(Map<String, String> persona){
  // romper la referencia
  Map<String, String>mapa = { ...persona };
  mapa['nombre3'] = persona['nombre3']?.toLowerCase() ?? 'No hay nombre';
  mapa['nombre'] = persona['nombre']?.toLowerCase() ?? 'No hay nombre';
  return mapa;
}

void main(List<String> args) {
  String nombre = 'a';
  String nombre2 = capitalizar(nombre);

  print(nombre);
  print(nombre2);

  Map<String, String> persona = {
    'nombre': 'a',
    'nombre2': 'a',
  };

  Map<String, String> persona2 = capitalizarMapa(persona);

  print(persona2);

}