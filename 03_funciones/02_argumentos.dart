void saludar(String mensaje) {
  print(mensaje);
}

void saludar2(String? mensaje) {
  print(mensaje);
}

// 2 paaramentro opcional
void saludar3(String mensaje,[String nombre = 'insertar nombre', int? edad]) {
  print('$mensaje $nombre - $edad');
}

void saludar4({
  String? mensaje,
  required String nombre,
  int veces = 10
}){
  print('saludar 2: $mensaje $nombre - $veces');
}

void saludar5(String mensaje, {
  required String nombre,
  int veces = 10
}){
  print('saludar 2: $mensaje $nombre - $veces');
}

void main() {
  saludar('Hola mundo');
  saludar2(null);
  saludar3('Hola mundo', 'nombre');
  saludar4(nombre: 'nombre', veces: 20);
  saludar5('saludos', nombre: 'nombre', veces: 20);

}