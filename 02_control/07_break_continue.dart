main() {
  for (int i = 0; i < 10; i++) {
    if(i == 5) {
      continue; // salta al a siguiente interacion
    }
    print(i);

    if (i == 2) {
      break; // salir del ciclo;
    }
  }
}