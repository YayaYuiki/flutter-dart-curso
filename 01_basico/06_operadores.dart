main(){
  int a = 10;
  dynamic b;

  b ??= 20; // Asignar el valor únicamente si la variable es null;

  print(b);

  // operadores condicionaels

  int c = 23;
  String respuesta = c > 25 ? 'C es mayor a 25' : 'C es menor a 25';

  print(respuesta);

  int d = b ?? a;
  print(d);

  // Operadores Relacionales
  // Todos retornan un valor booleano
  /*
    >  Mayor que
    <  Menor que
    >= Mayor o igual que
    <= Menor o igual que

    == Revisa si dos objetos son iguales
    != Revisa si dos objetos son diferentes

  */

  String p1 = 'Fernando';
  String p2 = 'Alberto';

  print(p1 == p2);

  int x = 20;
  int y = 30;

  print(x > y);
  print(x < y);
  print(x >= y);
  print(x <= y);

  // Operador de tipo
  int i = 10;
  String j = '10';

  print(i is int);
  print(j is int);
  print(j is! int); // js es diferente a entero
}