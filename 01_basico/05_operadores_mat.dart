/**
 * Un operador es un símbolo que le dice al compilador
 * que debe de realizar una tarea
 * matemática, relacional o lógica
 * y debe de producir un resultado
*/
main(){
  int a = 10 + 5; // + suma
  a = 20 - 10; // - resta
  a = 10 * 2; // * multiplicacion
  
  double b = 10 / 2; // / division
  b = 10 % 3; // % Els obrante de la division
  b = -b; // -expr Es usado para cambiar el valor de la expresión

  int c = 1 ~/ 3; // ~/ 3  la division comun y corriente solo el numero entero y no toma enc uenta el decimal

  int d = 1;
  d++; // ++ autoincrement
  d--; // -- desicrementar

  d += 2; // += 3
  d -= 2; // += 1  +=  /=
}