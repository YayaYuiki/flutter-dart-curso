main() {
  // ========== Números
  //var a = 10;
  // dynamic a = 10;
  int a = 10;
  double b = 5.5;

  // definir valor nullo
  int? c = null;

  int _a = 30;
  double $b = 40;

  double resultado = _a + $b;

  //print(resultado);

  // ========== string
  //var nombre = 'Aa';
  String nombre = 'Aa';
  String nombre2 = "Aa";
  String? nombre3;

  String contatenar = '$nombre $nombre2';

  String multilinea = '''
    Hola mundo
    ¡Cómo estás?
    $nombre
    0'Conoer''';

  print(nombre2);
  print(contatenar);

  // ========== Booleans
  //var isActive;
  bool isActive = true;
  bool isNotActive = !isActive;
  bool? isNull;
  print(isActive);
  //print(isNull!);

  // ========== Lists (Arrays, colecciones)
  // var villanos = ['Lex', 'Red Skull', 'Doom']
  List<String> villanos = ['Lex', 'Red Skull', 'Doom'];
  villanos[0] = 'otro';
  villanos.add('Duende verde');
  villanos.add('Duende verde');
  villanos.add('Duende verde');
  print(villanos);

  // emilinar duplicados

  // eliminar duiplicados de la lista
  var villanoesSet = villanos.toSet();
  print(villanoesSet.toList());

  // ========== Sets
  // var villanos2 = {'Lex', 'Red Skull', 'Doom'};
  Set<String> villanos2 = {'Lex', 'Red Skull', 'Doom'};

  villanos2.add('Duende verde');
  villanos2.add('Duende verde');
  villanos2.add('Duende verde');
  print(villanos2);
  var ab$cd = 10;
  // ========== Maps (Diccionarios / Objectos literales)
  /* var ironMan = {
    'nombre': 'Tony Stark',
    'poder': 'Inteligenci y el dinero',
    'nivel': 9000,
  }; */

  Map<String, dynamic> ironMan = {
    'nombre': 'Tony Stark',
    'poder': 'Inteligenci y el dinero',
    'nivel': 9000,
  };

  Map<int, dynamic> ironMan3 = {
    1: 'Tony Stark',
    2: 'Inteligenci y el dinero',
    3: 9000,
  };

  Map<double, dynamic> ironMan4 = {
    1.0: 'Tony Stark',
    1.5: 'Inteligenci y el dinero',
    3: 9000,
  };

  print(ironMan);
  print(ironMan['nivel']);
  print(ironMan4[4]);

  Map<String, dynamic> capitan = new Map();
  capitan.addAll({
    'nombre': 'Steve',
    'poder': 'Soportar',
    'nivel': 500,
  });

  capitan.addAll(ironMan);

  print(capitan);
}